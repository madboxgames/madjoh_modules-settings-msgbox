define([
	'require',
	'displayers/overlays/msgbox'
],
function(require, MsgBoxPage){
	var MsgBox = {
		page : null,

		init : function(){
			MsgBox.page = new MsgBoxPage();
			MsgBox.page.build(MsgBox);
		},
		show : function(params){
			if(!params.iconKey) params.iconKey = 'JUSTDARE_ICON_BROKEN';
			MsgBox.page.prepare(params);
			MsgBox.page.show();
		},
		hide : function(){
			MsgBox.page.hide();
		}
	};

	return MsgBox;
});